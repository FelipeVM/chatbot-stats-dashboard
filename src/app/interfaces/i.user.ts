export interface IUser {
    name: string;
    id: string;
    create: Date;
    client: string;
    type: string;
    sessionCode: string;
  }
  
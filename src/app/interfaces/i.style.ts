export interface IStyle {
    colors: {
      primary: string;
      secondary: string;
      tertiary: string;
    };
    images: {
      logo: string;
    };
  }
  
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import * as Chart from 'chart.js';
@Component({
  selector: 'app-cant-derive-chart',
  templateUrl: './cant-derive-chart.component.html',
  styleUrls: ['./cant-derive-chart.component.css']
})
export class CantDeriveChartComponent implements OnInit {

  public chart: any = null;

  public today: Date = new Date(new Date().toDateString());
  public monthStart: Date = new Date(new Date(new Date().setDate(1)).toDateString());
  public monthEnd: Date = this.today;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
	this.initializeChart();
  }

  private initializeChart(){
    Chart.defaults.global.plugins.datalabels.color = 'black';
		this.chart = new Chart('cantDerive', {
			type: 'pie',
			data: {
				labels: [],
				datasets: [
				  {
          data: [],
          backgroundColor: ['#77dd77', '#045904'],
				  }
				]
			  },
			  options: {
				  maintainAspectRatio: false,
				title: {
					display: true,
					text: 'Usuarios derivados a Agente'
				},
				tooltips: {
					enabled: true
				},
				legend: {
					display: true,
					position: 'bottom',
					labels: {
						fontColor: 'black'
					}
				}
			  }
		});
  }
  
  private showData(start: any, end: any): void {
		this.apiService.getCantDerived(start, end).subscribe(response=> {
			this.chart.data.labels = [];
			this.chart.data.datasets[0].data = [];
			for(var i=0;i<response.length;i++){
				this.chart.data.labels.push(response[i].Result);
				this.chart.data.datasets[0].data.push(response[i].Count);
			}
			this.chart.update();
		}, error => {
			console.error("Error de conexión");
		});
	}

  public onChange(args: any) {
		this.showData(args[0], args[1]);
	}

}
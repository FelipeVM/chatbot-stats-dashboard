import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import * as Chart from 'chart.js';
@Component({
  selector: 'app-cant-user-interactions-chart',
  templateUrl: './cant-user-interactions-chart.component.html',
  styleUrls: ['./cant-user-interactions-chart.component.css']
})
export class CantUserInteractionsChartComponent implements OnInit {

  private chart: any = null;

  private coloR = []; 
  public today: Date = new Date(new Date().toDateString());
  public monthStart: Date = new Date(new Date(new Date().setDate(1)).toDateString());
  public monthEnd: Date = this.today;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
	this.initializeChart();
  }
  public dynamicColors(){
	var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
  }

  private initializeChart(){
    Chart.defaults.global.plugins.datalabels.color = 'white';
		this.chart = new Chart('cantUserInteractions', {
			type: 'doughnut',
			data: {
				labels: [],
				datasets: [
				  {
          data: [],
          backgroundColor: this.coloR,
				  }
				]
			  },
			  options: {
				  maintainAspectRatio: false,
				cutoutPercentage: 90,
				title: {
					display: true,
					text: 'Interacciones'
				},
				tooltips: {
					enabled: true
				},
				legend: {
					display: true,
					position: 'bottom',
					labels: {
						fontColor: 'black'
					}
				}
			  }
		});
  }
  
  private showData(start: any, end: any): void {
		this.apiService.getCantUserInteractions(start, end).subscribe(response=> {
			this.chart.data.labels = [];
			this.chart.data.datasets[0].data = [];
			for(var i=1;i<=10;++i){
				this.chart.data.labels.push('Usuario ' + i);
				this.chart.data.datasets[0].data.push(response[i--].Count);
				this.coloR.push(this.dynamicColors());
			}
			this.chart.update();
		}, error => {
			console.error("Error de conexión");
		});
	}

  public onChange(args: any) {
		this.showData(args[0], args[1]);
	}

}
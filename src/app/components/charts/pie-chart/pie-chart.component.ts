import { Component, OnInit } from '@angular/core';
import { CLIENT_STYLE } from 'src/app/models/md.style';
import { IStyle } from 'src/app/interfaces/i.style';
import { GlobalService } from 'src/app/services/global/global.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  public STYLE_CLIENT: IStyle;
  constructor(private global: GlobalService) { }

  ngOnInit() {
    this.STYLE_CLIENT = CLIENT_STYLE[this.global.getCurrentUser().client];
  }

}

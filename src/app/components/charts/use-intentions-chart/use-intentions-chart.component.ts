import { Component} from '@angular/core';
import { Chart } from 'chart.js';
import 'chartjs-plugin-datalabels';
import { ApiService } from 'src/app/services/api.service';
import { IStyle } from 'src/app/interfaces/i.style';
import { GlobalService } from 'src/app/services/global/global.service';
import { CLIENT_STYLE } from 'src/app/models/md.style';


@Component({
  selector: 'app-root',
  templateUrl: './use-intentions-chart.component.html',
  styleUrls: ['./use-intentions-chart.component.css']
})

export class UseIntentionsChartComponent {

	public chart: any = null;
	
	public STYLE_CLIENT: IStyle;

	public today: Date = new Date(new Date().toDateString());
	public monthStart: Date = new Date(new Date(new Date().setDate(1)).toDateString());
  	public monthEnd: Date = this.today;

	constructor(private apiService: ApiService, private global: GlobalService) {
		
	}

	ngOnInit(): void {
		this.initializeChart();
	}
	
	private initializeChart(){
		Chart.defaults.global.plugins.datalabels.anchor = 'end';
		Chart.defaults.global.plugins.datalabels.align = 'end';
		Chart.defaults.global.plugins.datalabels.color = 'black';
		this.chart = new Chart('useIntentions', {
			type: 'horizontalBar',
			data: {
				labels: [],
				datasets: [
				  {
					label: 'Intenciones',
					fill: false,
					data: [],
					backgroundColor: '#77dd77',
					borderColor: '#77dd77'
				  },
				  {
					label: 'Porcentaje',
					fill: false,
					data: [],
					backgroundColor: '#045904',
					borderColor: '#045904'
				  }
				]
			  },
			  options: {
				maintainAspectRatio: false,
				title: {
					display: true,
					text: 'Cantidad de intenciones utilizadas'
				},
				tooltips: {
					enabled: true
				},
				legend: {
					display: true,
					position: 'bottom',
					labels: {
						fontColor: 'black'
					}
				},
				scales: {
				  yAxes: [{
					  ticks: {
						  fontColor: "black",
						  beginAtZero: true
					  }
				  }],
				  xAxes: [{
					ticks: {
						fontColor: "black",
						beginAtZero: true
					}
				  }]
				}
			  }
		});
		this.showData(this.monthStart, this.monthEnd);
	}

	private showData(start: any, end: any): void {
		this.apiService.getIntents(start, end).subscribe(response=> {
			this.chart.data.labels = [];
			this.chart.data.datasets[0].data = [];
			this.chart.data.datasets[1].data = [];
			for(var i=0;i<response.length;i++){
				if(!(response[i].count == 0)){
					this.chart.data.labels.push(response[i].title);
					this.chart.data.datasets[0].data.push(response[i].count);
					this.chart.data.datasets[1].data.push(response[i].percent);
				}
			}
			this.chart.update();
		}, error => {
			console.error("Error de conexión");
		});
	}
  
	public onChange(args: any) {
		this.showData(args[0], args[1]);
	}
}
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import * as Chart from 'chart.js';
@Component({
  selector: 'app-cant-consult-chart',
  templateUrl: './cant-consult-chart.component.html',
  styleUrls: ['./cant-consult-chart.component.css']
})
export class CantConsultChartComponent implements OnInit {

  public chart: any = null;

  public today: Date = new Date(new Date().toDateString());
  public monthStart: Date = new Date(new Date(new Date().setDate(1)).toDateString());
  public monthEnd: Date = this.today;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
	this.initializeChart();
  }

  private initializeChart(){
    Chart.defaults.global.plugins.datalabels.color = 'black';
		this.chart = new Chart('cantConsult', {
			type: 'pie',
			data: {
				labels: [],
				datasets: [
				  {
          data: [],
          backgroundColor: ['#77dd77', '#045904'],
				  }
				]
			  },
			  options: {
				maintainAspectRatio: false,
				title: {
					display: true,
					text: 'Cantidad de consultas Usuario vs Bot'
				},
				tooltips: {
					enabled: true
				},
				legend: {
					display: true,
					position: 'bottom',
					labels: {
						fontColor: 'black'
					}
				}
			  }
		});
  }
  
  private showData(start: any, end: any): void {
		this.apiService.getCantConsult(start, end).subscribe(response=> {
			this.chart.data.labels = [];
			this.chart.data.datasets[0].data = [];
			for(var i=0;i<response.length;i++){
				if(!(response[i].count == 0)){
					this.chart.data.labels.push(response[i].userSent);
					this.chart.data.datasets[0].data.push(response[i].mensajes);
				}
			}
			this.chart.update();
		}, error => {
			console.error("Error de conexión");
		});
	}

  public onChange(args: any) {
		this.showData(args[0], args[1]);
	}

}
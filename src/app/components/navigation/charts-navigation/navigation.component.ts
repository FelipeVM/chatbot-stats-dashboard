import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { IStyle } from 'src/app/interfaces/i.style';
import { CLIENT_STYLE } from 'src/app/models/md.style';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
}) 
export class NavigationComponent implements OnInit {

  public STYLE_CLIENT: IStyle;
  public dashboardOwner: String;
  public charts = [
    {
      id: '1',
      chart_name: 'Cantidad de intenciones utilizadas',
      chart_type: 'bar_chart',
      routerLink: 'use-intentions-chart'
    },
    {
      id: '2',
      chart_name: 'Calificación del usuario al bot',
      chart_type: 'pie_chart',
      routerLink: 'calification-chart'
    },
    {
      id: '3',
      chart_name: 'Consultas Usuario vs Bot',
      chart_type: 'pie_chart',
      routerLink: 'cant-consult-chart'
    },
    {
      id: '4',
      chart_name: 'Derivados a Agente',
      chart_type: 'pie_chart',
      routerLink: 'cant-derive-chart'
    },
    {
      id: '5',
      chart_name: 'Mensajes no entendidos por el bot',
      chart_type: 'pie_chart',
      routerLink: 'dont-understand-chart'
    },
  ];

  constructor(private global: GlobalService) { }

  ngOnInit() {
    this.STYLE_CLIENT = CLIENT_STYLE["everis"];
    this.dashboardOwner = "Everis";
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

}
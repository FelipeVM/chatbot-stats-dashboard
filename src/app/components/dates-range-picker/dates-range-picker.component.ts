import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { loadCldr, L10n, setCulture } from '@syncfusion/ej2-base';
import { DateRangePickerComponent } from '@syncfusion/ej2-angular-calendars';


declare var require: any;

loadCldr(
  require('cldr-data/supplemental/numberingSystems.json'),
  require('cldr-data/main/es-MX/ca-gregorian.json'),
  require('cldr-data/main/es-MX/numbers.json'),
  require('cldr-data/main/es-MX/timeZoneNames.json')
);


@Component({
  selector: 'app-dates-range-picker',
  templateUrl: './dates-range-picker.component.html',
  styleUrls: ['./dates-range-picker.component.css']
})

export class DatesRangePickerComponent implements OnInit, AfterViewInit {
  @ViewChild('ejDateRange', {static: false}) ejDateRange: DateRangePickerComponent;
  @Output() public dateRange = new EventEmitter<any>();

  public dateFormat: String = "yyyy-MM-dd";
  public today: Date = new Date(new Date().toDateString());
  public weekStart: Date = new Date(new Date(new Date().setDate(new Date().getDate() - (new Date().getDay() + 7) % 7)).toDateString());
  public weekEnd: Date = new Date(new Date(new Date().setDate(new Date(new Date().setDate((new Date().getDate()
    - (new Date().getDay() + 7) % 7))).getDate() + 6)).toDateString())
    ;
  public monthStart: Date = new Date(new Date(new Date().setDate(1)).toDateString());
  public monthEnd: Date = this.today;
  public lastStart: Date = new Date(new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setDate(1)).toDateString());
  public lastEnd: Date = this.today;
  public yearStart: Date = new Date(new Date(new Date().setDate(new Date().getDate() - 365)).toDateString());
  public yearEnd: Date = this.today;

  constructor() { }

  ngAfterViewInit() {
    this.ejDateRange.startDate = this.monthStart;
    this.ejDateRange.endDate = this.monthEnd;
  }

  ngOnInit() :void{
    setCulture("es-MX");
    L10n.load({
      'es-MX': {
        'daterangepicker': {
          placeholder: 'Seleccione un rango de fechas',
          today: 'Hoy',
          startLabel: 'Inicio',
          endLabel: 'Final',
          applyText: 'Aplicar',
          cancelText: 'Cancelar',
          selectedDays: 'Dias seleccionados',
          days: 'Dias',
          customRange: 'Rango personalizado',
        }
      }
    });
  }

  public onChange() {
    let date = [this.ejDateRange.startDate.getFullYear() + '-' + (this.ejDateRange.startDate.getMonth()+1) + '-' + this.ejDateRange.startDate.getDate(),
    this.ejDateRange.endDate.getFullYear() + '-' + (this.ejDateRange.endDate.getMonth() + 1) + '-' + this.ejDateRange.endDate.getDate()];
    this.dateRange.emit(date);
	}
}

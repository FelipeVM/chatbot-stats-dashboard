import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UseIntentionsChartComponent } from './components/charts/use-intentions-chart/use-intentions-chart.component';
import { CalificationChartComponent } from './components/charts/calification-chart/calification-chart.component';
import { CantConsultChartComponent } from './components/charts/cant-consult-chart/cant-consult-chart.component';
import { CantDeriveChartComponent } from './components/charts/cant-derive-chart/cant-derive-chart.component';
import { CantUserInteractionsChartComponent } from './components/charts/cant-user-interactions-chart/cant-user-interactions-chart.component';
import { DontUnderstandChartComponent } from './components/charts/dont-understand-chart/dont-understand-chart.component';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { NavigationComponent } from './components/navigation/charts-navigation/navigation.component';
import { DatesRangePickerComponent } from './components/dates-range-picker/dates-range-picker.component';
import { CustomMaterialModule } from './core/material.module';
import { HttpClientModule } from '@angular/common/http';
import { VerificationComponent } from './components/verification/verification.component';
import { PieChartComponent } from './components/charts/pie-chart/pie-chart.component';
import { ErrorComponent } from './pages/error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    UseIntentionsChartComponent,
    CalificationChartComponent,
    CantConsultChartComponent,
    CantDeriveChartComponent,
    CantUserInteractionsChartComponent,
    DontUnderstandChartComponent,
    NavigationComponent,
    DatesRangePickerComponent,
    VerificationComponent,
    PieChartComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DateRangePickerModule,
    CustomMaterialModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

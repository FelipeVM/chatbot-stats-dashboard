import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UseIntentionsChartComponent } from './components/charts/use-intentions-chart/use-intentions-chart.component';
import { CantConsultChartComponent } from './components/charts/cant-consult-chart/cant-consult-chart.component';
import { CalificationChartComponent } from './components/charts/calification-chart/calification-chart.component';
import { DontUnderstandChartComponent } from './components/charts/dont-understand-chart/dont-understand-chart.component';
import { CantDeriveChartComponent } from './components/charts/cant-derive-chart/cant-derive-chart.component';
import { CantUserInteractionsChartComponent } from './components/charts/cant-user-interactions-chart/cant-user-interactions-chart.component';
import { VerificationComponent } from './components/verification/verification.component';
import { ErrorComponent } from './pages/error/error.component';


const routes: Routes = [
  { path: '', redirectTo: 'use-intentions-chart', pathMatch: 'full' },
  { path: 'use-intentions-chart', component: UseIntentionsChartComponent },
  { path: 'cant-consult-chart', component: CantConsultChartComponent },
  { path: 'calification-chart', component: CalificationChartComponent },
  { path: 'dont-understand-chart', component: DontUnderstandChartComponent },
  { path: 'cant-derive-chart', component: CantDeriveChartComponent },
  { path: 'cant-user-interactions-chart', component: CantUserInteractionsChartComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

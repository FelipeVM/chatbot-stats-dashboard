import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // apiUrl: string = 'http://192.168.137.241:3000';
  apiUrl: string = 'https://dev-statistics-server.azurewebsites.net';

  constructor(private httpClient: HttpClient) { }

  public getIntents(start: any, end: any): Observable<any>{
    return this.httpClient.get(`${this.apiUrl}/intents?start=${start}&end=${end}`,
    {responseType: 'json'});
  }

  public getSessions(start: any, end: any): Observable<any>{
    return this.httpClient.get(`${this.apiUrl}/sessions?start=${start}&end=${end}`, {responseType: 'json'});
  }

  public getCantConsult(start: any, end: any): Observable<any>{
    return this.httpClient.get(`${this.apiUrl}/consult-none?start=${start}&end=${end}`, {responseType: 'json'});
  }

  public getCantDerived(start: any, end: any): Observable<any>{
    return this.httpClient.get(`${this.apiUrl}/not-derive?start=${start}&end=${end}`, {responseType: 'json'});
  }

  public getCalification(start: any, end: any): Observable<any>{
    return this.httpClient.get(`${this.apiUrl}/calification?start=${start}&end=${end}`, {responseType: 'json'});
  }

  public getCantUserInteractions(start: any, end: any): Observable<any>{
    return this.httpClient.get(`${this.apiUrl}/user-interactions?start=${start}&end=${end}`, {responseType: 'json'});
  }

  public getNotUnderstand(start: any, end: any): Observable<any>{
    return this.httpClient.get(`${this.apiUrl}/not-understand?start=${start}&end=${end}`, {responseType: 'json'});
  }
}

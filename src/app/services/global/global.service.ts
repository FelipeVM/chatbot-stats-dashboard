import { Injectable } from '@angular/core';
import { IUser } from 'src/app/interfaces/i.user';
import { IStyle } from 'src/app/interfaces/i.style';
import { v4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})

export class GlobalService {

  // tslint:disable-next-line: variable-name
  private _styleClient: IStyle;
  /**
   * Funcion que obtiene el estilo que pertenece ese cliente
   */
  get styleClient(): IStyle {
    return this._styleClient;
  }
  /**
   * Funcion que se encarga de enviar el estilo que corresponde al cliente
   */
  set styleClient(styleClient: IStyle) {
    this._styleClient = styleClient;
  }

  /**
   * Variable que controla el estado de una gente
   */
  private statusAgent = false;
  private token: string;
  public authorization = false;

  /**
   * Varaible que corresponde al usuario;
   */
  private currentUser: IUser;

  constructor() {
    this.createBasicUser();
  }

  /**
   * Funcion que se encraga de inicialziar un usuario simple
   */
  private createBasicUser() {
    const uuid = v4();
    this.currentUser = {
      name: ``,
      create: new Date(),
      type: 'user',
      client: 'everis',
      id: uuid,
      sessionCode: '',
    };
  }

  /**
   * Funcion que retorna el usuario actual
   */
  public getCurrentUser() {
    return this.currentUser;
  }

  /**
   * Funcion que asigna el cliente actual
   * @param client Valor del cliente actual
   */
  public setClient(client: string) {
    this.currentUser.client = client;
  }

  /**
   * Funcion que retorna el cliente actual
   */
  public get client(): string {
    return this.currentUser.client;
  }

  /**
   * Funcion que se encarga de actualizar el estado de un agente
   */
  public set agentStatus(status: boolean) {
    this.statusAgent = status;
  }

  /**
   * Funcion que se encarga de retornar el estado d eun agente
   */
  public get agentStatus(): boolean {
    return this.statusAgent;
  }

  /**
   * Funcion que se encarga de asignar un sessioncode al usuario
   */
  public setSessionCode(sessionCode: string) {
    if (this.currentUser.sessionCode !== sessionCode) {
      this.currentUser.sessionCode = sessionCode;
    }
  }

  /**
   * Funcion que se encarga de realizar un reset al usuario local
   */
  public resetUser() {
    const uuid = v4();
    this.setSessionCode('');
    this.currentUser = {
      name: ``,
      create: new Date(),
      type: 'user',
      client: this.client,
      id: uuid,
      sessionCode: '',
    };
  }

  /**
   * Funcion para poner el token
   * @param token Token de autorizacion
   */
  public setToken(token: string) {
    this.token = token;
  }

  /**
   * Funcion para obtener el token
   */
  public getToken(): string {
    return this.token;
  }
}

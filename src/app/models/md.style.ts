/**
 * Variable que corresponde a los estilos por cliente
 */
export const CLIENT_STYLE = {
  // ESTILOS Y PROPIEDADES CORRESPONDIENES A EVERIS
  everis: {
    colors: {
      primary: '#9AAE04',
      secondary: '#707372',
      tertiary: '#FFB81C',
      selected: 'rgba(35, 173, 7, 0.2)'
    },
    images: {
      logo: 'assets/everis/logo.jpg'
    }

  },
  // ESTILOS Y PROPIEDADES CORRESPONDIENES A PRIMAX
  primax: {
    colors: {
      primary: '#FF6900',
      secondary: '#1E22AA',
      tertiary: '#FFB81C',
      selected: 'rgba(205, 105, 0, 0.2)'
    },
    images: {
      logo: 'assets/primax/logo.svg'
    }
  },
  // ESTILOS Y PROPIEDADES CORRESPONDIENES A TERPEL
  terpel: {
    colors: {
      primary: '#FF0A19',
      secondary: '#1E22AA',
      tertiary: '#FFBB23',
      selected: 'rgba(255, 10, 25, 0.2)'
    },
    images: {
      logo: 'assets/terpel/logo.png'
    }
  },
  // ESTILOS Y PROPIEDADES CORRESPONDIENES A DEV
  desarrollo: {
    colors: {
      primary: '#2271b3',
      secondary: '#6500CB',
      tertiary: '#000068',
      selected: 'rgba(34, 113, 139, 0.2)'
    },
    images: {
      logo: 'assets/desarrollo/logo.png'
    }
  },
  // ESTILOS Y PROPIEDADES CORRESPONDIENES A PROVENIR
  porvenir: {
    colors: {
      primary: '#2271b3',
      secondary: '#6500CB',
      tertiary: '#000068',
      selected: 'rgba(34, 113, 139, 0.2)'
    },
    images: {
      logo: 'assets/desarrollo/logo.png'
    }
  },
  // ESTILOS Y PROPIEDADES CORRESPONDIENES A SENA
  sena: {
  colors: {
    primary: '#238276',
    secondary: '#596548',
    tertiary: '#fc7323l',
    selected: 'rgba(35, 130, 118, 0.2)'
  },
  images: {
    logo: 'assets/sena/logo.png'
  }
  }
};
